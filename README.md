# This is a client application built using React JS

## Requirnments

- Node - v10.16.2
- NPM - v6.9.0

## Installation and Usage guide

- Run `npm install` to install all dependencies for the application.

- Run the sever using any one of the following commands
    - `npm run dev-server` - This use's webpack-dev-server to launch the application in development mode.
    - `npm run prod-server` - This use's nodemon to launch the application.

## Asumptions 

- Just below the login button I have used another button to toggle login for users/admin. If the button says `Login in as Admin` then you can enter the admin email and password to login. Likewise if the button says `Login in as User` then you can enter the user email and password.

- Disabled the Approve/Reject button if the ticket is already in the same state.

- I used the same backend I made for the backend coding challenge you can find it [here](https://gitlab.com/srage13/coding-challenge). Please do pull the latest changes I added some additional validation to the backend repo as well.
