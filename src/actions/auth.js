import axios from 'axios';
import history from '../router/History'

export const updateLoginFormFields = (e) => {
    return(dispatch) => {
        dispatch({
            type: 'UPDATE_LOGIN_FORM_FIELDS',
            payload: {
                stateKey: e.target.id,
                stateValue: e.target.value
            } 
        });
    }
}

export const submitLoginForm = () => {
    return(dispatch, getState) => {
        axios({
            method: 'post',
            url: 'http://localhost:8080/api/user/login',
            data: {
                email: getState().auth.email,
                password: getState().auth.password
            }
        })
        .then((res) => {
            if(res.data.success) {
                if(getState().auth.loginRoleName == res.data.role) {
                    localStorage.setItem('accessToken', res.data.accessToken)
                    dispatch({
                        type: 'LOGIN_SUCCESSFULL',
                        payload: {
                            userId: res.data.userId,
                            accessToken: res.data.accessToken,
                            role: res.data.role,
                            firstName: res.data.firstName,
                            lastName: res.data.lastName,
                        } 
                    });
                } else {
                    dispatch({
                        type: 'LOGIN_ERROR',
                        payload: {
                            error: true,
                            message: "Could not login please check login type and try again"
                        } 
                    });
                }
            } else {
                dispatch({
                    type: 'LOGIN_ERROR',
                    payload: {
                        error: !res.data.success,
                        message: res.data.errorMessage
                    } 
                });
            }

        })
        .catch((error) => {
            console.log(error);
        });
    }
}

export const clearErrors = () => {
    return(dispatch) => {
        dispatch({
            type: 'CLEAR_ERROR'
        });
    }
}

export const updateLoginType = () => {
    return(dispatch) => {
        dispatch({
            type: 'TOGGLE_LOGIN_TYPE'
        });
    }
}

export const logout = (e) => {
    console.log(e)
    return(dispatch) => {
        dispatch({
            type: 'LOGOUT'
        });
    }
}


// export const logout = () => {
//     return(dispatch) => {
//         dispatch({
//             type: 'LOGOUT'
//         });
//     }
// }