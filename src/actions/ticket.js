import axios from 'axios';
import history from '../router/History'

export const addTicket = (formData) => {
    return(dispatch, getState) => {
        axios({
            method: 'post',
            url: 'http://localhost:8080/api/ticket',
            data: {
                summary: formData.summary,
                description: formData.description,
                type: formData.type,
                complexity: formData.complexity,
                estimatedtime: formData.estimatedtime,
                cost: formData.cost
            },
            headers: { Authorization: `Bearer ${getState().auth.accessToken}` }
        })
        .then((res) => {
            if(res.data.success) {
                dispatch({
                    type: 'CLEAR_FORM_FIELDS',
                });
            } else {
                dispatch({
                    type: 'FORM_SUBMIT_ERROR',
                    payload: {
                        error: !res.data.success,
                        message: res.data.errorMessage
                    } 
                });
                
            }
        })
        .catch((error) => {
            console.log(error);
        });
    }
}

export const resetFormSubmitState = () => {
    return(dispatch) => {
        dispatch({
            type: 'RESET_FORM_SUMBIT_STATE'
        });
    }
}

export const loadTicketList = () => {
    return(dispatch, getState) => {
        axios({
            method: 'get',
            url: 'http://localhost:8080/api/ticket/list',
            headers: { Authorization: `Bearer ${getState().auth.accessToken}` }
        })
        .then((res) => {
            if(res.data) {
                dispatch({
                    type: 'GET_TICKET_LIST',
                    payload: {
                        tickets: res.data
                    }
                });
            } else {
                dispatch({
                    type: 'ERROR_WHILE_FETCHING_DATA',
                    payload: {
                        error: true,
                        message: res.data.errorMessage
                    } 
                });
            }
        })
        .catch((error) => {
            console.log(error);
        });
    }
}

export const updateTicketStatus = (status, id) => {
    return(dispatch, getState) => {
        axios({
            method: 'put',
            url: `http://localhost:8080/api/ticket/edit/`+id,
            data: {
                status: status
            },
            headers: { Authorization: `Bearer ${getState().auth.accessToken}` }
        })
        .then((res) => {
            if(res.data) {
                dispatch({
                    type: 'TICKET_STATUS_UPDATED',
                });
            } else {
                dispatch({
                    type: 'FORM_SUBMIT_ERROR',
                    payload: {
                        error: !res.data.success,
                        message: res.data.errorMessage
                    } 
                });
                
            }
        })
        .catch((error) => {
            console.log(error);
        });
    }
}

export const resetTicketStatusUpdated = () => {
    return(dispatch) => {
        dispatch({
            type: 'RESET_TICKET_STATUS_UPDATED'
        });
    }
}
