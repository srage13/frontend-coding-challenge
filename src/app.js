import React from 'react';
import ReactDOM from 'react-dom';
import  {reduxStore, persistor}  from './redux-store/ReduxStore';
import { Provider } from 'react-redux';
import AppRouter from './router/AppRouter';
import { PersistGate } from 'redux-persist/integration/react'

const jsx = (
    <Provider store={reduxStore}>
        <PersistGate persistor = {persistor}>
            <AppRouter />
        </PersistGate>
    </Provider>
);

ReactDOM.render(jsx, document.getElementById('app'));