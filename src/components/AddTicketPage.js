import React, { useState, useEffect } from 'react';
import Header from './shared/Header';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { addTicket, resetFormSubmitState } from '../actions/ticket';

const AddTicketPage = (props) => {

    const [formData, setFormData] = useState({});
    const [redirectTo, setRedirectTo] = useState(null);
    const { handleSubmit, register, errors, setError } =  useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues: {},
        validateCriteriaMode: "firstError",
        submitFocusError: true,
        nativeValidation: false, // Note: version 3 only
    })

    const handleInputChange = (e) => {
        e.persist();
        setFormData((prevFormData) => ({ ...prevFormData, [e.target.id]: e.target.value }));
    }

    const submitForm = () => {
        let result = props.addTicket(formData)

    }

    useEffect(() => {
        if (props.ticket.formSumbitSuccess) {
            setFormData({});
            props.resetFormSubmitState()
            setRedirectTo(true);
        }
        return () => {
            setRedirectTo(false);
        };
    }, [props.ticket.formSumbitSuccess]);

    return (
        <>
            {redirectTo ? <Redirect to="/" /> : null}
            <Header />
            <h1 className="text-center p-3">Add Ticket</h1>
            <div className="container-fluid p-5 col-md-6 ticket-form-wrapper">
                <form onSubmit = {handleSubmit(submitForm)}>
                    <div className="form-group">
                        <label htmlFor="summary">Summary</label>
                        <input type="text" className="form-control" name = "summary" id="summary" placeholder="Summary" onChange={handleInputChange} defaultValue={formData.summary || ''} ref={register({required: 'Summary is required'})} />
                        <div className="error-message">{errors.summary && errors.summary.message}</div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <textarea className="form-control" id="description" name = "description" placeholder="Description" onChange={handleInputChange} defaultValue={formData.description || ''} ref={register({required: 'Description is required'})}></textarea>
                        <div className="error-message">{errors.description && errors.description.message}</div>
                    </div>
                    <div className="row">
                        <div className="form-group col-md-6">
                            <label htmlFor="type">Type</label>
                            <select id="type" className="custom-select" name = "type" onChange={handleInputChange} value={formData.type || ''} ref={register({required: 'Type is required'})}>
                                <option value="" disabled hidden >-- Select Type --</option>
                                <option value="enhancement" >Enhancement</option>
                                <option value="bug fix" >Bug Fix</option>
                                <option value="development" >Development</option>
                            </select>
                            <div className="error-message">{errors.type && errors.type.message}</div>
                        </div>
                        <div className="form-group col-md-6">
                            <label htmlFor="complexity">Complexity</label>
                            <select id="complexity" className="custom-select" name = "complexity" onChange={handleInputChange} value={formData.complexity || ''} ref={register({required: 'Complexity is required'})}>
                                <option value="" disabled hidden>-- Select Complexity --</option>
                                <option value="high" >High</option>
                                <option value="mid" >Mid</option>
                                <option value="low" >Low</option>
                            </select>
                            <div className="error-message">{errors.complexity && errors.complexity.message}</div>
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="estimatedtime">Estimated Time</label>
                        <input type="text" className="form-control" name = "estimatedtime" id="estimatedtime" placeholder="Estimated Time" onChange={handleInputChange} defaultValue={formData.estimatedtime || ''} ref={register({required: 'Estimated time is required'})}/>
                        <div className="error-message">{errors.estimatedtime && errors.estimatedtime.message}</div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="cost">Cost</label>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1">$</span>
                            </div>
                            <input type="text" className="form-control" name = "cost" id="cost" placeholder="Cost" onChange={handleInputChange} defaultValue={formData.cost || ''} ref={register({required: 'Estimated time is required', pattern: {value: /^[0-9]*$/,message: 'You cannot enter alphabets in this field'}})}/>
                        </div>
                        <div className="error-message">{errors.cost && errors.cost.message}</div>
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        </>
    )
}

const mapDispatchToProps = (dispatch) => ({
    addTicket: (formData) => dispatch(addTicket(formData)),
    resetFormSubmitState: () => dispatch(resetFormSubmitState())
});

const mapStateToProps = state => {
    return {
        ticket: state.ticket
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTicketPage);