import React, { useState, useEffect } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from './shared/Header';
import { loadTicketList } from '../actions/ticket';
import ListView from "./shared/ListView";

const ListPage = (props) => {

    const [tickets, setTickets] = useState({});
    const [filter, setFilters] = useState(null);

    const applyFilter = (param) => {
        switch(param) {
            case "type":
                if(filter.typeFilter != 'all')
                    setTickets(props.ticket.tickets.filter(ticket => ticket.type == filter.typeFilter))
                else 
                    setTickets(props.ticket.tickets)
                break;
            default: 
        }
    }
    
    const handleInputChange = (e) => {
        e.persist();
        setFilters((prevFilters) => ({ ...prevFilters, [e.target.id]: e.target.value }));
    }

    const sort = (param, orderAsc) => {
        switch(param) {
            case 'id':
                let newSortTicketsById = props.ticket.tickets.sort((a, b) => {
                    if(orderAsc) return b.ticketId - a.ticketId;
                    else return a.ticketId - b.ticketId;
                })
                setTickets(newSortTicketsById);
                break;
            case 'complexity':
                let newSortTicketsByComplexity = props.ticket.tickets.sort((a, b) => {
                    if(orderAsc) return a.complexity.localeCompare(b.complexity);
                    else return b.complexity.localeCompare(a.complexity);
                })
                setTickets(newSortTicketsByComplexity);
                break;
            default:
                break;
        }
    }

    useEffect(() => {
        props.loadTicketList();
    }, []);

    useEffect(() => {
        setTickets(props.ticket.tickets);
    }, [props.ticket.tickets]);

    return (
        <>
            <Header />
            <div className="container-fluid p-5">
                <div className="row">
                    <div className="col-md-8 col-xs-12 filters">
                        <label htmlFor="type">Type</label>
                        <select id="typeFilter" className="custom-select col-md-3" onChange = {handleInputChange}>
                            <option value="all" > View All </option>
                            <option value="enhancement" >Enhancement</option>
                            <option value="bug fix" >Bug Fix</option>
                            <option value="development" >Development</option>
                        </select>
                        <button className = "btn btn-primary" onClick = {() => applyFilter("type")}>APPLY FILTER</button>
                    </div>
                    <div className="col-md-4 col-xs-12">
                        {props.auth.role != "Admin" ? 
                        <Link to="/addticket" className="btn btn-primary float-right" >Add Ticket</Link>
                        : null}
                    </div>
                </div>
                <div className="col-md-12 col-xs-12 p-5" >
                    <ListView tickets = {tickets} sort = {(param, order) => sort(param, order)} />
                </div>
            </div>
        </>
    )
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
        ticket: state.ticket
    }
}

const mapDispatchToProps = (dispatch) => ({
    loadTicketList: () => dispatch(loadTicketList())
});

export default connect(mapStateToProps, mapDispatchToProps)(ListPage);