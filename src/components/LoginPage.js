import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { useForm } from 'react-hook-form';
import { updateLoginFormFields, submitLoginForm, clearErrors, updateLoginType } from '../actions/auth';
import { ToastContainer, toast } from 'react-toastify';


const LoginPage = (props) => {

    const { handleSubmit, register, errors, setError } =  useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues: {},
        validateCriteriaMode: "firstError",
        submitFocusError: true,
        nativeValidation: false, // Note: version 3 only
    })

    const submitForm = () => {
        props.submitLoginForm();
    }

    
    const { auth } = props;
    if (auth.userId != null && auth.accessToken != null ) return (<Redirect to='/' />);
    return (
        <div>
            <div className="login-form-wrapper my-5 pt-sm-5">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-8 col-lg-6 col-xl-5">
                            <div className="card overflow-hidden">
                                <div className="bg-soft-primary">
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="text-primary set-center p-4">
                                                <h5 className="text-primary">User Stories</h5>
                                                <p>Login in to continue.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body pt-0">
                                    <div className="p-2">
                                        <form className="form-horizontal"  onSubmit = {handleSubmit(submitForm)} >

                                            <div className="form-group">
                                                <label htmlFor="username">Email</label>
                                                <input type="text" className="form-control" name = "email" id="email" placeholder="Enter email" onChange={(e) => props.updateLoginFormFields(e)} defaultValue = {auth.email || ''} ref={register({required: 'Email is required', pattern: {value: /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i, message: 'Please enter a vaild email'}})} />
                                                <div className="error-message">{errors.email && errors.email.message}</div>
                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="userpassword">Password</label>
                                                <input type="password" className="form-control" name = "password" id="password" placeholder="Enter password" onChange={(e) => props.updateLoginFormFields(e)} defaultValue = {auth.password || ''} ref={register({required: 'Password is required'})} />
                                                <div className="error-message">{errors.password && errors.password.message}</div>
                                            </div>

                                            <div className="form-group mt-3">
                                                <button className="btn btn-primary btn-block" type="submit" >Log In</button>
                                            </div>

                                            <div className="form-group mt-3">
                                                <button className="btn btn-primary btn-block" type="button" onClick={(e) => props.updateLoginType(e)}>Log In As {auth.loginAsUser ? 'User': 'Admin'}</button>
                                                <p className = "text-info login-toggle-info">*Click here to toggle between user and admin login</p>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
    
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = (dispatch) => ({
    updateLoginFormFields: (e) => dispatch(updateLoginFormFields(e)),
    submitLoginForm: (e) => dispatch(submitLoginForm(e)),
    clearErrors: (e) => dispatch(clearErrors()),
    updateLoginType: () => dispatch(updateLoginType())
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);