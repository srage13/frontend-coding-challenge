import React, { useState, useEffect } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { updateTicketStatus, resetTicketStatusUpdated } from '../actions/ticket';
import Header from './shared/Header';
import { formatCurrency } from '../../lib/helper';

const STATUS_TEXT = {
    0: "PENDING",
    1: "APPROVED",
    2: "REJECTED"
}

const ReviewTicketPage = (props) => {

    const [selectedTicket, setSelectedTicket] = useState({});
    const [redirectTo, setRedirectTo] = useState(null);

    const openTicketViewPage = (ticketId) => {
        setSelectedTicketId(ticketId);
        setRedirectTo(true);
    }

    useEffect(() => {
        let id = props.match.params.id;
        Object.values(props.ticket.tickets).forEach((ticket) => {
            if (ticket.ticketId == id) setSelectedTicket(ticket);
        });
    }, []);

    useEffect(() => {
        if(props.ticket.ticketStatusUpdated) {
            props.resetTicketStatusUpdated()
            setRedirectTo(true);
        }
        return () => {
            setRedirectTo(false);
        };
    }, [props.ticket.ticketStatusUpdated]);

    

    const { complexity, cost, description, estimatedTime, status, summary, ticketId, type } = selectedTicket;
    return (
        <>
            {redirectTo ? <Redirect to="/" /> : null}
            <Header />
                <div className="container-fluid p-5 float-left table-responsive col-md-6">
                    <h3>Ticket #{ticketId}</h3>
                    <table className="table ticket-description">
                        <tbody>
                            <tr>
                                <th>Summary</th>
                                <td>{summary}</td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{description}</td>
                            </tr>
                            <tr>
                                <th>Type</th>
                                <td>{type}</td>
                            </tr>
                            <tr>
                                <th>Complexity</th>
                                <td>{complexity}</td>
                            </tr>
                            <tr>
                                <th>Estimated time for completion</th>
                                <td>{estimatedTime}</td>
                            </tr>
                            <tr>
                                <th>Cost</th>
                                <td>{formatCurrency(cost)}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{STATUS_TEXT[status]}</td>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
                <div className="card col-md-3 offset-md-2  p-5 review-options-wrapper">
                    <h4 className = "text-center">Options</h4>
                    <div className="card-body">
                        <button className = "btn btn-success form-control" onClick = {(status, id) => props.updateTicketStatus(1, ticketId)} disabled = {status == 1 ? true: false}>Approve</button><br/>
                        <button className = "btn btn-danger form-control"  onClick = {(status, id) => props.updateTicketStatus(2, ticketId)} disabled = {status == 2 ? true: false}>Reject</button>
                    </div>
                </div>
        </>
    )
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
        ticket: state.ticket
    }
}

const mapDispatchToProps = (dispatch) => ({
    updateTicketStatus: (status,id) => dispatch(updateTicketStatus(status,id)),
    resetTicketStatusUpdated: () => dispatch(resetTicketStatusUpdated())
});

export default connect(mapStateToProps, mapDispatchToProps)(ReviewTicketPage);