import React, { useState, useEffect } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { loadTicketList } from '../../actions/ticket';
import Ticket from "./Ticket"

const ListView = (props) => {

    const [ ticketIdDesc, setTicketIdDesc ] = useState(false);
    const [ complexityDesc, setComplexityDesc ] = useState(false);

    const {tickets} = props;

    const sortIdData = (param) => {
        props.sort(param,!ticketIdDesc);
        setTicketIdDesc(!ticketIdDesc);
    }

    const sortComplexityData = (param) => {
        props.sort(param,!complexityDesc);
        setComplexityDesc(!complexityDesc);
    }
    
    return (
        <>
            <div  className="table-responsive ticket-list">
                <table className="table">
                    <thead>
                        <tr>
                            <th onClick = {() => sortIdData("id")} scope="col">Id <i className={`fa ${ticketIdDesc ? 'fa-caret-up' : 'fa-caret-down'}`} aria-hidden="true"></i></th>
                            <th scope="col">Summary</th>
                            <th scope="col">Description</th>
                            <th scope="col">Type</th>
                            <th onClick = {() => sortComplexityData("complexity")} scope="col">Complexity <i className={`fa ${complexityDesc ? 'fa-caret-up' : 'fa-caret-down'}`} aria-hidden="true"></i></th>
                            <th scope="col">Estimated time for completion</th>
                            <th scope="col">Cost</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            typeof tickets !== undefined  ?
                            Object.values(tickets).map((ticket, i) => (
                                <Ticket key = {i} data = {ticket}/>
                            ))
                            : null
                        }
                    </tbody>
                </table>
            </div>
        </>
    )
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
    }
}

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(ListView);