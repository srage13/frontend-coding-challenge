import React, { useState, useEffect } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { loadTicketList } from '../../actions/ticket';
import {formatCurrency} from '../../../lib/helper'

const TICKET_STATUS = {
    0: 'pending',
    1: 'approved',
    2: 'rejected'
}

const Ticket = (props) => {

    const [ redirect, setRedirectTo ] = useState(false);
    const [ selectedTicketId, setSelectedTicketId ] = useState(null);

    const openTicketViewPage = (ticketId) => {
        setSelectedTicketId(ticketId);
        setRedirectTo(true);
    }

    const { complexity, cost, description, estimatedTime, status, summary, ticketId, type } = props.data;
    return (
        <>  
            {props.auth.role == 'Admin' ? redirect ? <Redirect to = {`/ticket/view/${selectedTicketId}`} /> : null : null}
            <tr onClick = {() => openTicketViewPage(ticketId)} className = {TICKET_STATUS[status]}>
                
                    <th scope="row">{ticketId}</th>
                    <td>{summary}</td>
                    <td>{description}</td>
                    <td>{type}</td>
                    <td>{complexity}</td>
                    <td>{estimatedTime}</td>
                    <td>{formatCurrency(cost)}</td>
            </tr>
        </>
    )
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
    }
}

export default connect(mapStateToProps)(Ticket);