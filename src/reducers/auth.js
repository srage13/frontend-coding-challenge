const initialAuthState = {
    userId: null,
    accessToken: null,
    role: null,
    firstName: null,
    lastName: null,
    loginAsUser: true,
    loginError: false,
    loginErrorMessage: null,
    loginRoleName: null
  }
  

export default (state = initialAuthState, action) => {
    switch (action.type) {
        case 'UPDATE_LOGIN_FORM_FIELDS':
            return { ...state, [action.payload.stateKey]: action.payload.stateValue }
        case 'LOGIN_SUCCESSFULL':
            return { ...state, userId: action.payload.userId, accessToken: action.payload.accessToken, role: action.payload.role, firstName: action.payload.firstName, lastName: action.payload.lastName }
        case 'LOGIN_ERROR':
            return { ...state, loginError: action.payload.error, loginErrorMessage: action.payload.message }
        case 'TOGGLE_LOGIN_TYPE':
            return { ...state, loginAsUser: !state.loginAsUser, loginRoleName: !state.loginAsUser?'User':'Admin' }
        case 'CLEAR_ERROR':
            return { ...state, loginError: false, loginErrorMessage: null, registerError: false, registerErrorMessage: null }
        case 'LOGOUT':
            return { ...state, userId: null, accessToken: null, role: null, firstName: null, lastName: null}
        default:
            return state;
    }
};
