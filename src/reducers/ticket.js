const initialTicketState = {
    tickets: {},
    formSumbitSuccess: false,
    formSubmitError: false,
    formErrorMessage: null,
    errorWhileFetchingData: null,
    ticketStatusUpdated: false
}
  

export default (state = initialTicketState, action) => {
    switch (action.type) {
        case 'CLEAR_FORM_FIELDS':
            return { ...state, formSumbitSuccess: true }
        case 'GET_TICKET_LIST':
            return { ...state, tickets: action.payload.tickets }
        case 'ERROR_WHILE_FETCHING_DATA':
            return { ...state, formSumbitSuccess: true }
        case 'RESET_FORM_SUMBIT_STATE':
            return { ...state, formSumbitSuccess: false }
        case 'FORM_SUBMIT_ERROR':
            return { ...state, formSubmitError: action.payload.error, formErrorMessage: action.payload.message }
        case 'TICKET_STATUS_UPDATED':
            return { ...state, ticketStatusUpdated: true }
        case 'RESET_TICKET_STATUS_UPDATED':
            return { ...state, ticketStatusUpdated: false }
        default:
            return state;
    }
};
