import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import authReducer from '../reducers/auth';
import ticketReducer from '../reducers/ticket';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['auth', 'ticket'] // For now using localstorage to persist auth data.
}

const rootReducer = combineReducers({
  auth: authReducer,
  ticket: ticketReducer
});

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const reduxStore = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(thunk))
);
export  const persistor = persistStore(reduxStore)

export default { reduxStore, persistor }
