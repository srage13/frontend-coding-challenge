import React from 'react';
import { BrowserRouter as Router, Route, Switch, Link, NavLink } from 'react-router-dom';
import history from './History';
import LoginPage from '../components/LoginPage';
import ListPage from '../components/ListPage';
import AddTicketPage from '../components/AddTicketPage';
import ReviewTicketPage from '../components/ReviewTicketPage';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';

const AppRouter = () => (
  <Router history={history}>
    <div>
      <Switch>
        <PublicRoute path="/login" component={LoginPage} exact={true} initial />
        <PrivateRoute path="/" component={ListPage} exact  />
        <PrivateRoute path="/ticket/view/:id" component={ReviewTicketPage} exact  />
        <PrivateRoute path="/" component={ListPage} exact  />
        <PrivateRoute path="/addticket" component={AddTicketPage} exact  />
        {/* <PublicRoute path="/" component={404PageNotFound}  /> */}
      </Switch>
    </div>
  </Router>
);

export default AppRouter;
