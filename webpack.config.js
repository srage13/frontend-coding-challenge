'use strict';

const webpack = require('webpack');
const path = require('path');

module.exports = (env) => {
	let config = {
	  	entry: [
			'./src/app.js'
        ],
	  	output: {
			path: path.join(__dirname, 'public'),
			publicPath: '/public/',
            filename: 'bundle.js'  
		},
		// Webpack watch exits for some reason on debian/ubuntu based systems, this config makes sure that the watch option
		// does not exit automatically https://github.com/webpack/webpack/issues/2949
		watchOptions: {
			poll: true
		},
		devServer: {
			publicPath: './',
			contentBase: path.join(__dirname, 'public'),
    		historyApiFallback: true,
			port: 3000,
			writeToDisk: true
		},
		
	  	module: {
	        // Set up for babel
			rules: [
				{
					test: /\.(js|jsx)$/,
					exclude:path.resolve(__dirname, 'node_modules'),
					use: {
					loader: 'babel-loader',
					options: {
						presets: [
								'@babel/preset-env',
								'@babel/preset-react'
							],
							plugins : [
								["@babel/plugin-proposal-decorators", { "legacy": true }],
								'@babel/plugin-syntax-dynamic-import',
								['@babel/plugin-proposal-class-properties', { "loose": true }]
							]
						}
					},
				}
			]
	    },
	    // Set the default number of chunks created to one. 
		plugins: [

		],
		externals: {
	        
		},
	};

	return config;
}